FROM ubuntu

MAINTAINER https://gitlab.com/dedmin

ENV SQUID_USER=proxy \
    SQUID_CONF=/etc/squid/squid.conf \
    SQUID_CACHE_DIR=/var/spool/squid \
    SQUID_LOG_DIR=/var/log/squid \
    SQUID_TZ=Europe/Moscow
    
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install --no-install-recommends -y squid && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY conf/squid.conf "$SQUID_CONF"
COPY entrypoint.sh /usr/local/bin

RUN chmod 644 "$SQUID_CONF" && \
    chmod 755 /usr/local/bin/entrypoint.sh && \
    addgroup "$SQUID_USER" tty

EXPOSE 3128/tcp

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
