# Docker-squid

[![Pipeline Status](https://gitlab.com/dedmin/squid/badges/master/pipeline.svg)](https://gitlab.com/dedmin/squid/commits/master) 

# Introduction

This is automated build of squid for runing in docker container

# Squid

Squid is a caching proxy for the Web supporting HTTP, HTTPS, FTP, and more. It reduces bandwidth and improves response times by caching and reusing frequently-requested web pages. Squid has extensive access controls and makes a great server accelerator.

# Building

Run it in command line:

```bash
git clone https://gitlab.com/dedmin/squid.git
cd squid
docker build . -t dedmin/squid
```

# Configuration

Run container on the host system and mount directories and configuration file:

```bash
sudo bash squid-run.sh
or
docker pull dedmin/squid
docker run -d --name squid --restart always dedmin/squid
```